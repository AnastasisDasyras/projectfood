$(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});

//show hide text in BLENDS
$(document).ready(function(){
    if($( window ).width() < 768){
        document.querySelector('.blends-paragraph-1').textContent = "There are many variations of passages of Lorem Ipsum...";
        document.querySelector('.blends-paragraph-2').textContent = "There are many variations of passages of Lorem Ipsum...";
        document.querySelector('.blends-paragraph-3').textContent = "There are many variations of passages of Lorem Ipsum...";
    }

});
$( window ).resize(function() {
    if($( window ).width() < 768){
        document.querySelector('.blends-paragraph-1').textContent = "There are many variations of passages of Lorem Ipsum...";
        document.querySelector('.blends-paragraph-2').textContent = "There are many variations of passages of Lorem Ipsum...";
        document.querySelector('.blends-paragraph-3').textContent = "There are many variations of passages of Lorem Ipsum...";
    }
    else{
        document.querySelector('.blends-paragraph-1').textContent = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.";
        document.querySelector('.blends-paragraph-2').textContent = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.";
        document.querySelector('.blends-paragraph-3').textContent = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.";
    }
});